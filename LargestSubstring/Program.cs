﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LargestSubstring
{
    class Program
    {
        static void Main(string[] args)
        {
            string cadena;
            int largestSubstring;
            Console.WriteLine("Introduzca una cadena");
            cadena = Console.ReadLine();
            largestSubstring = LengthOfLongestSubstring(cadena);
            Console.WriteLine(largestSubstring);
            Console.Read();
        }

        public class Cell
        {
            public int Position { get; set; }
            public string Character { get; set; }
        }
        private static int LengthOfLongestSubstring(string s)
        {
            switch (s.Length)
            {
                case 0:
                case 1:
                    return s.Length;
                case 2:
                    if (s[0] == s[1])
                        return 1;
                    else
                        return 2;
                case 3:
                    if (s[0] == s[1] && s[0] == s[2])
                        return 1;
                    else if ((s[1] != s[0] && s[1] == s[2]) || (s[0] == s[1] && s[0] != s[2]) || (s[2] == s[0] && s[2] != s[1]))
                        return 2;
                    else
                        return 3;
                default:
                    break;
            }
            int j = s.Length;
            int actualCell = 0;
            int pos = 0;
            Cell previous = new Cell();
            Cell cell = new Cell();
            Cell previousRepeater = new Cell();
            List<Cell> previousChars = new List<Cell>();
            int previousPosition = 0, previousValue = 0;
            int nextValue = 0;
            Dictionary<Cell, int> map = new Dictionary<Cell, int>();

            for (int x = 0; x < j; x++)
            {
                if (!map.Any(m => m.Key.Character == s.Substring(x, 1)))
                {
                    if (map.Count == 0 || cell.Position == 0)
                    {
                        if (previousChars.Count > 0)
                            map.Add(new Cell() { Character = s.Substring(x, 1), Position = x }, x - (cell.Position > previousChars.FirstOrDefault().Position ? cell.Position : previousChars.FirstOrDefault().Position));
                        else
                            map.Add(new Cell() { Character = s.Substring(x, 1), Position = x }, x + 1);
                    }
                    else
                    {
                        if (previousChars.Count > 0)
                            map.Add(new Cell() { Character = s.Substring(x, 1), Position = x }, x - (cell.Position > previousChars.FirstOrDefault().Position ? cell.Position : previousChars.FirstOrDefault().Position));
                        else
                            map.Add(new Cell() { Character = s.Substring(x, 1), Position = x }, x + 1);
                    }
                }
                else
                {
                    cell = map.FirstOrDefault(m => m.Key.Character == s.Substring(x, 1)).Key;
                    previous = map.FirstOrDefault(m => m.Key.Character == s.Substring(x - 1, 1)).Key;
                    previousValue = previous == null ? previousValue : map[previous];
                    actualCell = map[cell];
                    pos = previousPosition == 0 ? cell.Position : previousPosition;
                    if (previousChars.Count > 0)
                    {
                        if (cell.Position > previousChars.FirstOrDefault().Position)
                            nextValue = x - cell.Position;
                        else
                            nextValue = x - previousChars.FirstOrDefault().Position;
                    }
                    else
                    {
                        if (pos == 0)
                            nextValue = x - pos;
                        else
                            nextValue = ((x - pos) + 1);
                    }
                    map.Remove(cell);
                    if (previous != null && previous == cell && cell.Position > 0 && previousPosition == 0)
                        map.Add(new Cell() { Character = cell.Character, Position = x }, pos + 1 > actualCell ? pos + 1 : actualCell);
                    else
                        map.Add(new Cell() { Character = cell.Character, Position = x }, nextValue > actualCell ? nextValue : actualCell == 0 ? cell.Position : actualCell);
                    previousRepeater = new Cell() { Position = cell.Position, Character = cell.Character };
                    previousChars.Add(previousRepeater);
                    previousPosition = x;
                }
            }
            return map.Values.Max();
        }
    }
}
